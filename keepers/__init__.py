# -*- coding: utf-8 -*-

from __future__ import absolute_import
from .version import __version__

# this h5py import is a workaround for this bug:
# https://github.com/PythonCharmers/python-future/pull/278
try:
    import h5py
except ImportError:
    pass

from .dependency_injector import *

from .configuration import *
from .logging import *
from .dynamic_parametrization import *
from .versioning import *


get_Configuration = ConfigurationManager.get_Configuration
