
from builtins import object


class Variable(object):
    def __init__(self, name, default_value_list, checker, genus='str'):
        self.name = str(name)
        if genus in ['str', 'int', 'float', 'boolean']:
            self.genus = str(genus)
        if not isinstance(default_value_list, list):
            raise ValueError("The default_value_list argument must be a list!")
        elif len(default_value_list) == 0:
            default_value_list = [None]
        self.default_value_list = default_value_list

        if callable(checker):
            self.checker = checker
        else:
            raise ValueError("The checker must be callable!")

        self.set_value(None)

    def set_value(self, value=None):
        if value is None:
            work_list = self.default_value_list
        else:
            work_list = [value]
        success = False
        for item in work_list:
            if self.checker(item):
                valid_item = item
                success = True
                break
        if not success:
            raise ValueError("No valid value supplied: " + str(work_list))
        else:
            self.value = valid_item
            return self.value

    def __call__(self):
        return self.get_value()

    def get_value(self):
        return self.value

    def get_name(self):
        return self.name

    def __repr__(self):
        return "<" + str(self.name) + "': " + \
               str(self.get_value()) + ">"
