# -*- coding: utf-8 -*-

CALL_TAG = []


def tag(tag):
    def wrap(f):
        def wrapped_f(*args, **kwargs):
            global CALL_TAG
            CALL_TAG += [tag]
            f(*args, **kwargs)
            CALL_TAG.pop()
        return wrapped_f
    return wrap
