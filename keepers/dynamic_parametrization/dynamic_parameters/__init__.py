# -*- coding: utf-8 -*-

from __future__ import absolute_import
from .dynamic_parameter import DynamicParameter
from .parent_call_parameter import ParentCallParameter
