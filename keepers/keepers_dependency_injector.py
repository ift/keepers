# -*- coding: utf-8 -*-

from .dependency_injector import DependencyInjector

# Setup the dependency injector
keepers_dependency_injector = DependencyInjector(
                                    ['h5py',
                                     ('mpi4py.MPI', 'MPI'),
                                     ('mpi_dummy', 'MPI_dummy')]
                                     )
