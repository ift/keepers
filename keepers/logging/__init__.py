# -*- coding: utf-8 -*-

from __future__ import absolute_import
from .mpi_logger import MPILogger
from .loggable import Loggable
