# -*- coding: utf-8 -*-

from builtins import object
import logging


class classproperty(object):

    def __init__(self, fget):
        self.fget = fget

    def __get__(self, owner_self, owner_cls):
        return self.fget(owner_cls)


class Loggable(object):
    @classproperty
    def logger(cls):
        try:
            cls._logger
        except AttributeError:
            name = cls.__name__
            cls._logger = logging.getLogger(name)
        finally:
            return cls._logger
