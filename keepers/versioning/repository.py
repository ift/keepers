# -*- coding: utf-8 -*-

# from builtins import str
from builtins import range
from builtins import object
import os
import sys
import random
import string
import time
from contextlib import contextmanager


from keepers.versioning.versionable import Versionable
from keepers.versioning.versionable_wrapper import VersionableWrapper

from keepers.keepers_dependency_injector import keepers_dependency_injector

try:
    MPI = keepers_dependency_injector['MPI']
except KeyError:
    MPI = keepers_dependency_injector['MPI_dummy']

h5py = keepers_dependency_injector.get('h5py')


class Repository(object):
    def __init__(self, file_path='repository.hdf5', comm=MPI.COMM_WORLD):
        self.comm = comm
        # set helper objects for file handle
        self._file_handle = None
        self._file_ref_counter = 0

        self._objects_for_next_commit = {}
        self._stored_objects = []
        self._loaded_objects = {}

        self.file_path = os.path.abspath(file_path)

        try:
            h5py_parallel = h5py.get_config().mpi
            if h5py_parallel:
                h5py.File(self.file_path,
                          mode='r',
                          driver='mpio',
                          comm=self.comm).close()
            else:
                h5py.File(self.file_path, mode='r').close()
        except(IOError):
            self._initialize_file(self.file_path)
        # set active_commit to the latest commit
        self.checkout(self.latest_commit)

    # ---Technical helper functions---

    def _initialize_file(self, file_path):
        directory = os.path.dirname(file_path)
        if not os.path.exists(directory):
            os.makedirs(directory)

        # touch the file
        h5py_parallel = h5py.get_config().mpi
        if h5py_parallel:
            h5py.File(file_path,
                      mode='a',
                      driver='mpio',
                      comm=self.comm).close()
        else:
            h5py.File(file_path, mode='a').close()

        # preform an empty commit in order to initialize the file structure
        self.commit()

    def _create_global_commit_id(self):
        if self.comm.rank == 0:
            time_part = str(int(time.time()))
            random_part = ''.join(random.choice(string.ascii_lowercase)
                                  for i in range(6))
            global_commit_id = time_part + '_' + random_part
        else:
            global_commit_id = None

        global_commit_id = self.comm.bcast(global_commit_id, root=0)
        return global_commit_id

    @contextmanager
    def _repository_file(self):
        h5py_parallel = h5py.get_config().mpi
        if self.comm.size > 1 and not h5py_parallel:
            raise RuntimeError("ERROR: Programm is run with MPI " +
                               "size > 1 but non-parallel version of " +
                               "h5py is loaded.")
        try:
            if self._file_handle is None:
                if h5py_parallel:
                    self._file_handle = h5py.File(self.file_path,
                                                  mode='r+',
                                                  driver='mpio',
                                                  comm=self.comm)
                else:
                    self._file_handle = h5py.File(self.file_path, mode='r+')
            self._file_ref_counter += 1
            yield self._file_handle

        finally:
            self._file_ref_counter -= 1
            if self._file_ref_counter == 0:
                self._file_handle.close()
                self._file_handle = None

    # ---User interaction methods and properties---

    def add(self, obj, name):
        if not isinstance(obj, Versionable):
            obj = VersionableWrapper(obj)
        self._objects_for_next_commit[name] = obj

    def remove(self, name):
        del(self._objects_for_next_commit[name])

    def checkout(self, commit):
        with self._repository_file() as f:
            if commit not in list(f['commits'].keys()):
                raise ValueError("Given commit id could not be found in "
                                 "repository file.")

            # Add the names of the registered variables to the
            # _objects_for_next_commit list, such that the hard-link
            # references will be created during the next commits
            for name in f['/commits/' + commit + '/name_to_id'].attrs:
                self._objects_for_next_commit[name] = None

        self.active_commit = str(commit)
        self._loaded_objects = {}

    @property
    def parent_commit(self):
        with self._repository_file() as f:
            active_commit = f['/commits/'+self.active_commit]
            parent_commit_str = active_commit.attrs['parent_commit']
        return parent_commit_str

    @property
    def latest_commit(self):
        with self._repository_file() as f:
            latest_commit = f.attrs['latest_commit']
        return latest_commit

    @property
    def object_names(self):
        with self._repository_file() as f:
            name_to_id_path = '/commits/' + self.active_commit + '/name_to_id'
            name_to_id = f[name_to_id_path].attrs
            object_names = list(name_to_id.keys())
        return object_names

    @property
    def object_ids(self):
        with self._repository_file() as f:
            commit_group_path = '/commits/' + self.active_commit + '/objects'
            object_ids = list(f[commit_group_path].keys())
        return object_ids

    @property
    def commits(self):
        with self._repository_file() as f:
            commits = list(f['/commits/'].keys())
        return commits

    # ---Store methods---

    def commit(self):
        with self._repository_file() as f:
            # create a new group in 'commits'
            commit_id = self._create_global_commit_id()
            commit_group = f.create_group('commits/'+commit_id)

            # set parent_commit
            try:
                commit_group.attrs['parent_commit'] = self.active_commit
            except(AttributeError):
                # if self.active_commit does not exist since this is the first
                # commit, set a loop
                commit_group.attrs['parent_commit'] = \
                    os.path.split(commit_group.name)[-1]

            # update the active_commit attribute
            self.active_commit = commit_id

            # prepare group for name<->id mapping
            name_to_id = commit_group.create_group('name_to_id')

            # store objects that are in _objects_for_next_commit
            # Thereby store the explicitly given objects first. Afterwards add
            # links to the objects from the parent commit.
            # If one mixes up the order, the objects from parent can block
            # the storing of an object amendment

            for name, obj in self._objects_for_next_commit.items():
                if obj is not None:
                    self.store_object(obj=obj, name=name)

            # now create hardlinks for all implicictly stored objects
            objects_to_store = []
            # first of all get the obj_ids for the explicit object names
            # from the last commit
            for name, obj in self._objects_for_next_commit.items():
                if obj is None:
                    try:
                        # try to get obj and its id from the parent commit
                        parent_name_to_id = f['/commits/' +
                                              self.parent_commit +
                                              '/name_to_id']
                    except(ValueError):
                        raise ValueError("Object could not be found in "
                                         "parent commit.")
                    else:
                        obj_id = parent_name_to_id.attrs[name]
                        objects_to_store += [obj_id]
                        # even if the obj was already stored, if it was an
                        # amendment, its name will not have been registered
                        name_to_id.attrs[name] = obj_id

            # make a stack approach in order to recursively resolve all
            # dependencies
            while len(objects_to_store) > 0:
                obj_id = objects_to_store.pop()
                if obj_id not in self._stored_objects:
                    # store obj / its name<->id into the current commit
                    obj_dset = f['/commits/' + self.parent_commit +
                                 '/objects/' + obj_id]
                    commit_group['objects/' + obj_id] = obj_dset
                    self._stored_objects += [obj_id]
                    objects_to_store += \
                        list(obj_dset['name_to_id'].attrs.values())

            f.attrs['latest_commit'] = commit_id

        # reset the _objects_for_next_commit and the _stored_objects dicts
        for key in list(self._objects_for_next_commit.keys()):
            self._objects_for_next_commit[key] = None
        self._stored_objects = []
        self._loaded_objects = {}

    def store_object(self, obj, name=None, source_group=None):
        # store the obj
        obj_id = obj._get_versioning_id()
        obj_group_path = ('/commits/' +
                          self.active_commit +
                          '/objects/' +
                          obj_id)
        amendments = None
        if obj_id not in self._stored_objects:
            with self._repository_file() as f:
                obj_group = f.create_group(obj_group_path)
                obj_class = obj.__class__
                obj_group.attrs['__class'] = obj_class.__name__
                obj_group.attrs['__module'] = obj_class.__module__
                f.create_group(obj_group_path + '/name_to_id')
                amendments = obj._to_hdf5(obj_group)
            self._stored_objects += [obj_id]

        # Check if the object is already registered under the given name in the
        # group that requested to store the object
        if name is not None:
            if source_group is None:
                source_group = '/commits/' + self.active_commit
            with self._repository_file() as f:
                name_to_id = f[source_group + '/name_to_id'].attrs
                if name not in name_to_id:
                    name_to_id[name] = obj_id

        # If obj returned a dictionary of additional objects, store them
        # and put the name_to_id information in the objects name_to_id object
        if amendments is not None:
            for (amend_name, amend_obj) in amendments.items():
                self.store_object(amend_obj,
                                  name=amend_name,
                                  source_group=obj_group_path)
        return obj_id

    # ---Get/load methods---

    def __getitem__(self, name):
        return self.get(name)

    def get(self, name, target_group=None):
        with self._repository_file() as f:
            # if no target group is given, the name_to_id information will
            # be taken from the root of the commit.
            if target_group is None:
                target_group_name = '/commits/' + self.active_commit
            else:
                target_group_name = target_group.name
            name_to_id = f[target_group_name + '/name_to_id'].attrs
            identifier = name_to_id[name]

        loaded_object = self.load_object(identifier)
        return loaded_object

    def load_object(self, identifier):
        if identifier not in self._loaded_objects:
            with self._repository_file() as f:
                obj_group = f['/commits/' +
                              self.active_commit +
                              '/objects/' +
                              identifier]
                obj_class = self._reconstruct_class(obj_group)
                # Create a dummy instance of the loaded object in order to
                # register its future id to the _loaded_objects dictionary.
                # It is important to do this *before* _from_hdf5 is called such
                # that obj can resolve eventual references to itself.
                dummy = self._get_dummy_instance(obj_class)
                self._loaded_objects[identifier] = dummy

                # actually load the object from the hdf5 file
                loaded_object = obj_class._from_hdf5(obj_group, self)

                # put the content of loaded _object into the dummy
                self._set_dummy_content(dummy, loaded_object)

        loaded_object = self._loaded_objects[identifier]

        # unpack the loaded object if the original object was not Versionable
        if isinstance(loaded_object, VersionableWrapper):
            loaded_object = loaded_object.unwrap()

        return loaded_object

    def _reconstruct_class(self, committed_object):
        obj = committed_object
        class_name = obj.attrs['__class']
        module_name = obj.attrs['__module']
        __import__(module_name)
        mod = sys.modules[module_name]
        loaded_class = getattr(mod, class_name)
        return loaded_class

    def _get_dummy_instance(self, obj_class):
        class empty_temp(obj_class):
            def __init__(self):
                pass
        dummy_instance = empty_temp()
        return dummy_instance

    def _set_dummy_content(self, dummy, loaded_object):
        dummy.__class__ = loaded_object.__class__
        dummy.__dict__ = loaded_object.__dict__
