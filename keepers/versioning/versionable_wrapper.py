# -*- coding: utf-8 -*-

from __future__ import absolute_import
from .versionable import Versionable


class VersionableWrapper(Versionable):
    def __init__(self, data):
        self.data = data

    def _to_hdf5(self, hdf5_group):
        hdf5_group.attrs['data'] = self.data

    @classmethod
    def _from_hdf5(cls, hdf5_group, loopback_get):
        data = hdf5_group.attrs['data']
        result = cls(data)
        return result

    def unwrap(self):
        return self.data
