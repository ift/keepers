# -*- coding: utf-8 -*-

import os
from setuptools import setup, find_packages

exec(open('keepers/version.py').read())


# Utility function to read the README file.
# Used for the long_description.  It's nice, because now 1) we have a top level
# README file and 2) it's easier to type in the README file than to put a raw
# string in below ...
def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "keepers",
    version = __version__,
    author = "Theo Steininger",
    author_email = "theos@mpa-garching.mpg.de",
    description = ("A tool for logging, configuration, dependency injection "
                   "and saving."),
    license = "BSD",
    keywords = "",
    url = "https://gitlab.mpcdf.mpg.de/ift/keepers",
    packages=find_packages(),
    package_dir={'keepers': 'keepers'},
    #long_description=read('README'),
    zip_safe=False,
    dependency_links = [
    "git+https://gitlab.mpcdf.mpg.de/ift/mpi_dummy.git#egg=mpi_dummy-1.0.0"],
    install_requires=['mpi_dummy>=1.0.0', 'future', 'configparser'],
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Topic :: Utilities",
        "License :: OSI Approved :: BSD License",
    ],
)